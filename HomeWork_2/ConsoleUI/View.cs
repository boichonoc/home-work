﻿using _I_O_Terminal;
using System;
using System.Collections.Generic;


namespace HomeWork_2
{
    class View
    {
        private static readonly View instance = new View();
        public static View getInstance()
        {
            return instance;
        }
        public void Menu(ref Terminal terminal)
        {
            bool breakThread = true;
            while ( breakThread != false )
            {
                terminal.Write("------------- Menu ------------\r\n" +
                               "1) Store: Press to 'S'\r\n" +
                               "2) Rectangle: Press to 'R'\r\n" +
                               "3) City: Press to 'C'\r\n" +
                               "-------------------------------\r\n");

                switch (terminal.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        breakThread = false;
                        break;
                    case ConsoleKey.S:
                        InternetStore(terminal);
                        break;
                    case ConsoleKey.R:
                        Rectangle(terminal);
                        break;
                    case ConsoleKey.C:
                        City(terminal);
                        break;
                }
            }

         }
        private void City(Terminal terminal)
        {
            terminal.Write( "Enter your city...\r\n" +
                            "Title\r\n" +
                            "latitude\r\n" +
                            "longitude\r\n" );

            City city = new City();
            city.location = new GeoLocation();

            city.SetCityInfo(terminal.Read(), terminal.Read(), terminal.Read());
            city.PrintCityInfo(ref terminal);
        }
        private void Rectangle(Terminal terminal)
        {
            terminal.Write("\r\nSet side of rectangle");
            Rectangle rectangle = new Rectangle(double.Parse(terminal.Read()), double.Parse(terminal.Read()));
            terminal.Write("\r\nPerimeter: " + rectangle.Perimeter.ToString());
            terminal.Write("\r\nArea: " + rectangle.Area.ToString());
        }
        private void InternetStore(Terminal terminal)
        {
            Account beneficiarysAccount = new Account("UA213223130000026007233566001", 1000);
            Account payersCarrentAccount = new Account("UA253443260000046227233566223", 1000);

            List<Order> listOrders = new List<Order>();
            List<Worker> listWorkers = new List<Worker>();
            List<Price> listPrices = new List<Price>();

            bool breakThread = true;

            while (breakThread != false)
            {
                terminal.Write("\r\n" +
                "\r\nQ - New Order" +
                "\r\nW - New Worker" +
                "\r\nE - New Product" +
                "\r\nR - Print list of orders" +
                "\r\nT - Print list of workers" +
                "\r\nY - Print list of Products");

                switch (terminal.ReadKey(true).Key)
                {
                    case ConsoleKey.Backspace:
                        breakThread = false;
                        break;
                    case ConsoleKey.Q:
                        AddNewOrder(ref terminal, ref listOrders, ref payersCarrentAccount, ref beneficiarysAccount);
                        break;
                    case ConsoleKey.W:
                        AddNewWorker(ref terminal, ref listWorkers);
                        break;
                    case ConsoleKey.E:
                        AddNewProduct(ref terminal, ref listPrices);
                        break;
                    case ConsoleKey.R:
                        PrintListOfOrders(ref terminal, ref listOrders);
                        break;
                    case ConsoleKey.T:
                        PrintListOfWorkers(ref terminal, ref listWorkers);
                        break;
                    case ConsoleKey.Y:
                        PrintListOfProducts(ref terminal, ref listPrices);
                        break;
                }

            }
        }
        private void AddNewOrder(ref Terminal terminal, ref List<Order> listOrders, ref Account payersCarrentAccount, ref Account beneficiarysAccount)
        {
            terminal.Write("\r\n");
            Order newOrder = new Order();
            newOrder.CreateListOfOrders(ref terminal, ref listOrders, ref newOrder, ref payersCarrentAccount, ref beneficiarysAccount);
        }
        private void AddNewWorker(ref Terminal terminal, ref List<Worker> listWorkers)
        {
            terminal.Write("\r\n");
            Worker newWorker = new Worker();
            newWorker.CreateListOfWorkers(ref terminal, ref listWorkers, ref newWorker);
        }
        private void AddNewProduct(ref Terminal terminal, ref List<Price> listPrices)
        {
            terminal.Write("\r\n");
            Price newPrice = new Price();
            newPrice.CreatePriceList(ref terminal, ref listPrices, ref newPrice);
        }
        private void PrintListOfOrders(ref Terminal terminal, ref List<Order> listOrders)
        {
            terminal.Write("Please set min limit... \r\n");
            double limit = double.Parse(terminal.Read());
            foreach (Order order in listOrders)
            {
                order.ShowOrdersCashTransferUpperThan(ref terminal, limit);
            }
        }
        private void PrintListOfWorkers(ref Terminal terminal, ref List<Worker> listWorkers)
        {
            terminal.Write("Set a minimum years of joining...");
            int limit = int.Parse(terminal.Read());
            foreach (Worker worker in listWorkers)
            {
                worker.ShowExperienceHigherThan(ref terminal, limit);
            }
        }
        private void PrintListOfProducts(ref Terminal terminal, ref List<Price> listPrices)
        {
            terminal.Write("Enter product name...");
            string name = terminal.Read();
            foreach (Price price in listPrices)
            {
                price.ShowProductInfo(ref terminal, name);
            }
        }
    }
}
