﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_2
{
    class Rectangle
    {
        private double side1, side2;

        public double Area
        {
            get
            {
                return this.AreaCalculator();
            }
        }

        public double Perimeter
        {
            get
            {
                return this.PerimeterCalculator();
            }
        }

        public Rectangle(double side1, double side2)
        {
            this.side1 = side1;
            this.side2 = side2;
        }
        
        public void SetFirstSide(double value)
        {
            this.side1 = value;
        }
        public void SetSecondSide(double value)
        {
            this.side1 = value;
        }
        public double GetFirstSide()
        {
            return this.side1;
        }
        public double GetSecondSide()
        {
            return this.side2;
        }
        private double PerimeterCalculator()
        {
            return 2 * (this.side1 + this.side2);
        }

        private double AreaCalculator()
        {
            return this.side1 * this.side2;
        }

    }
}
