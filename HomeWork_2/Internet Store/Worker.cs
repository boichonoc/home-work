﻿using System;
using System.Collections.Generic;
using _I_O_Terminal;

namespace HomeWork_2
{
    class Worker : IComparable<Worker>
    {
        private string firstName;
        private string secondName;
        private string currentPosition;
        private int yearOfJoining;
        
        public void SetFirstName(string firstName)
        {
            this.firstName = firstName;
        }

        public void SetSecondName(string secondName)
        {
            this.secondName = secondName;
        }

        public void SetCurrentPosition(string currentPosition)
        {
            this.currentPosition = currentPosition;
        }

        public void SetYearOfJoining(int yearOfJoining)
        {
            this.yearOfJoining = yearOfJoining;
        }

        public string GetFirstName()
        {
            return this.firstName;
        }

        public string GetSecondName()
        {
            return this.secondName;
        }

        public string GetCurrentPosition()
        {
            return this.currentPosition;
        }

        public int GetYearOfJoining()
        {
            return this.yearOfJoining;
        }

        private void SortBySecondName(List<Worker> workers)
        {
            workers.Sort();
        }

        public void CreateListOfWorkers(ref Terminal terminal, ref List<Worker> workers, ref Worker worker)
        {
            terminal.Write("First Name:");
            worker.SetFirstName(terminal.Read());

            terminal.Write("Second Name:");
            worker.SetSecondName(terminal.Read());

            terminal.Write("Current Position:");
            worker.SetCurrentPosition(terminal.Read());

            terminal.Write("Year of joining:");
            worker.SetYearOfJoining(int.Parse(terminal.Read()));

            workers.Add(worker);
            SortBySecondName(workers);

        }

        public void ShowExperienceHigherThan(ref Terminal terminal, int limit)
        {
            if(this.GetYearOfJoining() >= limit)
            {
                terminal.Write("First Name:");
                terminal.Write(this.GetFirstName());

                terminal.Write("Second Name:");
                terminal.Write(this.GetSecondName());

                terminal.Write("Current Position:");
                terminal.Write(this.GetCurrentPosition());

                terminal.Write("Year of joining:");
                terminal.Write(this.GetYearOfJoining());
            }
        }


        public int CompareTo(Worker compareWorker)
        {
            if (compareWorker == null)
                return 1;
            else
                return this.secondName.CompareTo(compareWorker.secondName);
        }
    }
}
