﻿using System;
using System.Collections.Generic;
using _I_O_Terminal;

namespace HomeWork_2
{
    class Price : IComparable<Price>
    {
        private string nameProduct;
        private string nameStore;
        private double price;

        public void SetProductName(string name)
        {
            this.nameProduct = name;
        }
        public void SetStoreName(string name)
        {
            this.nameStore = name;
        }
        public void SetPrice(double price)
        {
            this.price = price;
        }

        public string GetProductName()
        {
            return this.nameProduct;
        }
        public string GetStoreName()
        {
            return this.nameStore;
        }
        public double GetPrice()
        {
            return this.price;
        }
        public void CreatePriceList(ref Terminal terminal, ref List<Price> prices, ref Price price)
        {
            terminal.Write("Set Product Name:");
            price.SetProductName(terminal.Read());

            terminal.Write("Set Store Name:");
            price.SetStoreName(terminal.Read());

            terminal.Write("Set Price:");
            price.SetPrice(double.Parse(terminal.Read()));

            prices.Add(price);
            SortByProductName(prices);

        }
        public void ShowProductInfo(ref Terminal terminal, string name)
        {
            if( this.nameProduct == name)
            {
                terminal.Write("Product:");
                terminal.Write("       :" + this.GetProductName());
                terminal.Write("Store:");
                terminal.Write("     :" + this.GetStoreName());
                terminal.Write("Price:");
                terminal.Write("     :" + this.GetPrice() + " uah");
            }
        }
        private void SortByProductName(List<Price> prices)
        {
            prices.Sort();
        }
        public int CompareTo(Price comparePrice)
        {
            if (comparePrice == null)
                return 1;
            else
                return this.price.CompareTo(comparePrice.price);
        }
    }
}
