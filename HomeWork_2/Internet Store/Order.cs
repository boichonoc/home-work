﻿using System;
using System.Collections.Generic;
using _I_O_Terminal;

namespace HomeWork_2
{
    struct Account
    {
        private string aCountryCode;
        private uint controlNumbers;
        private uint bankCode;
        private string bankAccountNuber;
        private double cash;

        public Account(string IBAN, double cash)
        {
            this.aCountryCode = IBAN.Substring(0, 2);
            this.controlNumbers = uint.Parse(IBAN.Substring(2, 2));
            this.bankCode = uint.Parse(IBAN.Substring(4, 6));
            this.bankAccountNuber = IBAN.Substring(10, 19);
            this.cash = cash;
        }
        public string GetAccount()
        {
            return string.Concat(this.aCountryCode,
                                 this.controlNumbers.ToString(),
                                 this.bankCode.ToString(),
                                 this.bankAccountNuber);
        }
        public string GetCountryCode()
        {
            return this.aCountryCode;
        }
        public uint GetControlNumber()
        {
            return this.controlNumbers;
        }
        public uint GetBankCode()
        {
            return this.bankCode;
        }
        public string GetBankAccountNumber()
        {
            return this.bankAccountNuber;
        }
        public string GetCashAccount(double rate, string currency)
        {
            return string.Format("{0:f2}", (this.cash / rate)) + " " + currency;
        }
        public void PutCashToAccount(double value, double rate)
        {
            this.cash += value * rate;
        }
        public void PullCashFromAccount(double value, double rate)
        {
            this.cash -= value * rate;
        }
    }
    class Order : IComparable<Order>
    {
        private Account payersCarrentAccount;
        private Account beneficiarysAccount;
        private double cashTransfer;
        private string name;

        readonly double uahRate = 1.0;
        readonly double usdRate = 28.57;
        readonly double eurRate = 33.44;

        public void SetPayersCarrentAccount(ref Account payersCarrentAccount)
        {
            this.payersCarrentAccount = payersCarrentAccount;
        }
        public void SetBeneficiarysAccount(ref Account beneficiarysAccount)
        {
            this.beneficiarysAccount = beneficiarysAccount;
        }
        public void SetNameOrder(string name)
        {
            this.name = name;
        }
        public Account GetPayersCarrentAccount()
        {
            return this.payersCarrentAccount;
        }
        public Account GetBeneficiarysAccount()
        {
            return this.beneficiarysAccount;
        }
        public string GetNameOrder()
        {
            return this.name;
        }
        public double GetUahRate()
        {
            return this.uahRate;
        }
        public double GetUsdRate()
        {
            return this.usdRate;
        }
        public double GetEurRate()
        {
            return this.eurRate;
        }


        public void SetCashTransfer(double value, double rate)
        {
            this.cashTransfer = value * rate;
        }
        public string GetCashTransfer(string currency, double rate)
        {
            return string.Format("{0:f2}", (this.cashTransfer / rate)) + " " + currency;
        }

        public int CompareTo(Order compareOrder)
        {
            if (compareOrder == null)
                return 1;
            else
                return this.cashTransfer.CompareTo(compareOrder.cashTransfer);
        }

        public void CreateListOfOrders(ref Terminal terminal, ref List<Order> orders, ref Order order, ref Account payersCarrentAccount, ref Account beneficiarysAccount)
        {

            terminal.Write("Name Order:");
            order.SetNameOrder(terminal.Read());
            terminal.Write("     " + order.GetNameOrder());

            order.SetPayersCarrentAccount(ref payersCarrentAccount);
            terminal.Write("Payers Carrent Accoun:");
            terminal.Write("     " + order.GetPayersCarrentAccount().GetAccount());
            terminal.Write("     " + order.GetPayersCarrentAccount().GetCashAccount(order.GetUsdRate(), "usd"));

            order.SetBeneficiarysAccount(ref beneficiarysAccount);
            terminal.Write("Payers Beneficiarys Accoun:");
            terminal.Write("     " + order.GetBeneficiarysAccount().GetAccount());
            terminal.Write("     " + order.GetBeneficiarysAccount().GetCashAccount(order.GetUsdRate(), "usd"));

            terminal.Write("Cash transfer: ");
            order.SetCashTransfer(double.Parse(terminal.Read()), order.GetUsdRate());

            orders.Add(order);

            SortDescending(orders);

            terminal.Write("New order was added.");
        }

        private void SortDescending(List<Order> orders)
        {
            orders.Sort();
            orders.Reverse();
        }

        public void ShowOrdersCashTransferUpperThan(ref Terminal terminal, double limit)
        {
            string temp = this.GetCashTransfer("usd", this.GetUsdRate());
            if (double.Parse(temp.Substring(0, temp.Length - 3)) >= limit)
            {
                terminal.Write("Order name: ");
                terminal.Write("     " + this.GetNameOrder());
                terminal.Write("Beneficiarys Account: ");
                terminal.Write("     " + this.beneficiarysAccount.GetAccount());
                terminal.Write("PayersCarrent Account: ");
                terminal.Write("     " + this.payersCarrentAccount.GetAccount());
                terminal.Write("Cash Transfer: ");
                terminal.Write("     " + this.GetCashTransfer("usd", this.GetUsdRate()));
                terminal.Write("\r\n");
            }
        }
    }

}
