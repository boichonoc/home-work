﻿using System;
using System.Collections.Generic;
using _I_O_Terminal;

namespace HomeWork_2
{
    class Program
    {
        //"UA213223131111126007233566001"
        //"UA253443260000046227233566223"
        //"48° 27' 0.00\" N"
        //"34° 58' 59.99\" E"

        static void Main(string[] args)
        {
            Terminal terminal = Terminal.getInstance();
            View view = View.getInstance();

            view.Menu(ref terminal);

        }
    }
}
