﻿using System;

namespace _I_O_Terminal
{
    class Terminal
    {
        private static  readonly Terminal instance = new Terminal();
        
        public static Terminal getInstance()
        {
            return instance;
        }
        public void Write(int value)
        {
            Console.WriteLine(value);
        }
        public void Write(double value)
        {
            Console.WriteLine(value);
        }
        public void Write(string str)
        {
            Console.WriteLine(str);
        }

        public string Read()
        {
            return Console.ReadLine();
        }
        public ConsoleKeyInfo ReadKey()
        {
            return Console.ReadKey();
        }
        public ConsoleKeyInfo ReadKey(bool state)
        {
            return Console.ReadKey(state);
        }
    }
}
