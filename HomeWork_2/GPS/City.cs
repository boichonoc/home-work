﻿using System;
using System.Collections.Generic;
using System.Text;
using _I_O_Terminal;

namespace HomeWork_2
{
    class City
    {
        private string title;
        public GeoLocation location = null;
        
        public void PrintCityInfo(ref Terminal terminal)
        {
            string temp =  "I love " + this.title + "! Located at ";
            terminal.Write(temp + "(" + this.location.GetLatitude() + "; " + this.location.GetLongitude() + ")");
        }
        public void SetCityInfo(string title, string latitude, string longitude)
        {
            this.title = title;
            this.location.SetLatitude(latitude);
            this.location.SetLongitude(longitude);
        }

    }
}
