﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_2
{
    struct Coordinates
    {
        private int degree;
        private int minute;
        private float second;
        private char direct;

        public Coordinates(string coordinate)
        {
            this.degree = int.Parse(coordinate.Substring(0, coordinate.IndexOf("°")));
            this.minute = int.Parse(coordinate.Substring(coordinate.IndexOf("°") + 2, coordinate.IndexOf("'") - 4));
            this.second = float.Parse(coordinate.Substring(coordinate.IndexOf("'") + 2, coordinate.IndexOf("\"") - 8));
            this.direct = coordinate[coordinate.IndexOf("\"") + 2];
        }

        public void SetDegree(int degree)
        {
            this.degree = degree;
        }

        public int GetDegree()
        {
            return this.degree;
        }

        public void SetMinute(int minute)
        {
            this.minute = minute;
        }
        public int GetMinute()
        {
            return this.minute;
        }
        public void SetSecond(float sec)
        {
            this.second = sec;
        }
        public float GetSecond()
        {
            return this.second;
        }
        public void SetDirect(char dir)
        {
            this.direct = dir;
        }
        public char GetDirect()
        {
            return this.direct;
        }
        
    }
    class GeoLocation
    {
        private Coordinates Longitude;
        private Coordinates Latitude;

        public void SetLongitude(string longitude)
        {
            this.Longitude = new Coordinates(longitude);
        }
        public void SetLatitude(string latitude)
        {
            this.Latitude = new Coordinates(latitude);
        }
        public string GetLongitude()
        {
            string degree = this.Longitude.GetDegree().ToString() + "°";
            string minute = this.Longitude.GetMinute().ToString() + "'";
            string sec = this.Longitude.GetSecond().ToString() + "\"";
            string direct = this.Longitude.GetDirect().ToString();

            return degree + " " + minute + " " + sec + " " + direct;
        }
        public string GetLatitude()
        {
            string degree = this.Latitude.GetDegree().ToString() + "°";
            string minute = this.Latitude.GetMinute().ToString() + "'";
            string sec = this.Latitude.GetSecond().ToString() + "\"";
            string direct = this.Latitude.GetDirect().ToString();

            return degree + " " + minute + " " + sec + " " + direct;
        }
    }
}
